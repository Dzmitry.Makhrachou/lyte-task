from abc import ABC, abstractmethod


class BaseExternalApiService(ABC):
    @property
    @abstractmethod
    def uri(self):
        return ''
