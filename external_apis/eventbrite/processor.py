from dateutil import parser
from django.db import transaction

from lyte_app.constants import EventTicketTypes
from lyte_app.models import Organizer, Event, EventTicket

# TODO: Functionality to be adjusted/added:
# 1) Check on existing entities in DB to get rid of duplicates
# 2) Bulk create/update
# 3) Refactoring for better readability after


class EventbriteEventsProcessor:
    def __init__(self, events, *args, **kwargs):
        self.events = events
        self.processed_events = None
        self.processed_organizers = None
        self.processed_tickets = None

    def _prepare_organizer(self, name):
        return Organizer(name=name)

    def _prepare_event(self, name, date_string, organizer):
        start_date = parser.parse(date_string)
        return Event(
            name=name,
            start_date=start_date,
            organizer=organizer,
        )

    def _prepare_ticket(self, event, price):
        return EventTicket(
            event=event,
            type=EventTicketTypes.REGULAR.value,
            price=price,
        )

    def process(self):
        with transaction.atomic():
            for e in self.events:
                event_name = e['name']['text']
                event_start_date = e['start']['utc']
                organizer_name = e['organizer']['name']
                event_price = e['ticket_availability']['minimum_ticket_price']['value']

                organizer = self._prepare_organizer(organizer_name)
                organizer.save()

                event = self._prepare_event(event_name, event_start_date, organizer)
                event.save()

                event_ticket = self._prepare_ticket(event, event_price)
                event_ticket.save()
