import requests

from external_apis.base import BaseExternalApiService


class EventbriteApiService(BaseExternalApiService):
    @property
    def uri(self):
        return 'https://www.eventbriteapi.com/v3'

    def _format_uri(self, path):
        # TODO: change after Eventbrite starts working
        # return self.uri + path
        return 'https://private-anon-a530975be7-eventbriteapiv3public.apiary-mock.com/v3/events/search/?token=MDP6ANMHIGNYCY5D2AM3'

    @property
    def _headers(self):
        return {
            # 'Authorization': 'Bearer {}'.format('BZUF4DDBGPUZRAOUXFKC'),
            'Authorization': 'Bearer {}'.format('MDP6ANMHIGNYCY5D2AM3'),
        }

    def get_events(self):
        uri = self._format_uri('/events/search/')
        response = requests.get(uri, headers=self._headers)
        return response.json()['events']
