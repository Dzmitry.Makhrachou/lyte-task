import django
django.setup()

from external_apis.eventbrite.api_service import EventbriteApiService
from external_apis.eventbrite.processor import EventbriteEventsProcessor


if __name__ == '__main__':
    events = EventbriteApiService().get_events()
    processor = EventbriteEventsProcessor(events)
    processor.process()
