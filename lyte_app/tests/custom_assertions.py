from unittest import TestCase

from dateutil import parser


class CustomAssertionsMixin(TestCase):
    def assertDateTimeEqual(self, response_string, expected_datetime):
        response_datetime = parser.parse(response_string)
        try:
            self.assertEqual(response_datetime.year, expected_datetime.year),
            self.assertEqual(response_datetime.month, expected_datetime.month),
            self.assertEqual(response_datetime.day, expected_datetime.day),
            self.assertEqual(response_datetime.hour, expected_datetime.hour),
            self.assertEqual(response_datetime.minute, expected_datetime.minute),
            self.assertEqual(response_datetime.second, expected_datetime.second),
        except AssertionError:
            raise AssertionError(
                'Date {} is not equal with {}'.format(response_datetime, expected_datetime)
            )
