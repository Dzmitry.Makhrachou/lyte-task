from datetime import timedelta

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from lyte_app.tests.custom_assertions import CustomAssertionsMixin
from lyte_app.tests.factories import EventFactory, EventTicketFactory


class EventsViewReadTestCase(CustomAssertionsMixin, APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.event = EventFactory()
        cls.ticket = EventTicketFactory(event=cls.event)
        cls.list_path = '/v1/events/'
        cls.retrieve_path = f'/v1/events/{cls.event.id}/'

    def _test_keys_in_response(self, expected, actual):
        return self.assertCountEqual(expected, actual)

    def test_list(self):
        response = self.client.get(self.list_path, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = response.data
        self.assertEqual(len(response_data), 1)
        response_event = response_data[0]
        self.assertEqual(response_event['id'], self.event.id)
        self.assertEqual(response_event['name'], self.event.name)
        self.assertDateTimeEqual(response_event['start_date'], self.event.start_date)

    def test_list__expected_data_keys(self):
        expected_response_data_keys = [
            'id', 'name', 'start_date'
        ]
        response = self.client.get(self.list_path, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self._test_keys_in_response(
            expected_response_data_keys,
            response.data[0].keys(),
        )

    def test_retrieve__exists(self):
        response = self.client.get(self.retrieve_path, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = response.data

        self.assertEqual(response_data['id'], self.event.id)
        self.assertEqual(response_data['name'], self.event.name)
        self.assertDateTimeEqual(response_data['start_date'], self.event.start_date)

        self.assertEqual(response_data['organizer']['name'], self.event.organizer.name)
        self.assertEqual(response_data['organizer']['id'], self.event.organizer.id)

        tickets = response_data['tickets']
        self.assertEqual(len(tickets), 1)
        ticket = tickets[0]
        self.assertEqual(ticket['id'], self.ticket.id)
        self.assertEqual(ticket['price'], self.ticket.price / 100)

    def test_retrieve__not_exists(self):
        path = self.list_path + str(self.event.id + 1)

        response = self.client.get(path, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve__expected_data_keys(self):
        expected_response_data_keys = [
            'id', 'name', 'start_date', 'organizer', 'tickets'
        ]
        response = self.client.get(self.retrieve_path, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self._test_keys_in_response(
            expected_response_data_keys,
            response.data.keys(),
        )


class EventsViewUpdateTestCase(CustomAssertionsMixin, APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.list_path = '/v1/events/'

    def _generate_path(self, pk):
        return self.list_path + str(pk)

    def test_update__name(self):
        event = EventFactory()
        new_name = 'some fake name'
        data = {
            'name': new_name,
        }

        response = self.client.put(
            self._generate_path(event.id), data=data, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], new_name)

    def test_update__start_date(self):
        event = EventFactory()
        new_start_date = event.start_date + timedelta(days=1)
        data = {
            'start_date': new_start_date,
        }

        response = self.client.put(
            self._generate_path(event.id), data=data, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDateTimeEqual(response.data['start_date'], new_start_date)

    def test_update__date_in_the_past(self):
        event = EventFactory()
        data = {
            'start_date': timezone.now() - timedelta(days=1)
        }

        response = self.client.put(
            self._generate_path(event.id), data=data, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertTrue('start_date' in response.data)

    def test_update__organizer(self):
        event = EventFactory()
        new_name = 'some fake name'
        data = {
            'organizer': {
                'id': event.organizer.id,
                'name': new_name,
            }
        }

        response = self.client.put(
            self._generate_path(event.id), data=data, format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['organizer']['name'], new_name)


class EventsViewFilterTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.list_path = '/v1/events/'

    def test_ticket_price(self):
        event1 = EventFactory()
        EventTicketFactory(price=10000, event=event1)
        event2 = EventFactory()
        EventTicketFactory(price=50000, event=event2)

        response = self.client.get(
            self.list_path, {'max_ticket_price': 5000}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

        response = self.client.get(
            self.list_path, {'max_ticket_price': 25000}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(
            self.list_path, {'max_ticket_price': 55000}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_start_date(self):
        now = timezone.now()
        in_a_week = now + timedelta(days=7)
        in_two_weeks = now + timedelta(days=14)
        in_three_weeks = now + timedelta(days=21)
        EventFactory(start_date=in_a_week)
        EventFactory(start_date=in_two_weeks)
        EventFactory(start_date=in_three_weeks)

        response = self.client.get(
            self.list_path,
            {'start_date_after': now + timedelta(days=6)}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

        response = self.client.get(
            self.list_path,
            {'start_date_after': now + timedelta(days=13)}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        response = self.client.get(
            self.list_path,
            {'start_date_after': now + timedelta(days=15)}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        response = self.client.get(
            self.list_path,
            {
                'start_date_after': now + timedelta(days=13),
                'start_date_before': now + timedelta(days=15),
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

