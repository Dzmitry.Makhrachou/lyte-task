import random
from datetime import timedelta

import factory
from django.utils import timezone

from lyte_app.models import Event, Organizer, EventTicket


class OrganizerFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Organizer


class EventFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')
    organizer = factory.SubFactory(OrganizerFactory)

    class Meta:
        model = Event

    @factory.lazy_attribute
    def start_date(self):
        now = timezone.now()
        in_a_month = now + timedelta(days=30)
        in_a_year_and_month = in_a_month.replace(year=in_a_month.year + 1)

        return factory.Faker(
            'date_time_between_dates',
            datetime_start=in_a_month,
            datetime_end=in_a_year_and_month,
            tzinfo=timezone.get_current_timezone()
        ).generate({})


class EventTicketFactory(factory.DjangoModelFactory):
    price = factory.LazyFunction(lambda: random.randint(1000, 50000))
    event = factory.SubFactory(EventFactory)

    class Meta:
        model = EventTicket
