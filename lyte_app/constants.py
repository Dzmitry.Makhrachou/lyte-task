from lyte_app.base import BaseEnum


class EventTicketTypes(BaseEnum):
    REGULAR = 'Regular', 1
    EXTENDED = 'Extended', 2
    VIP = 'VIP', 3
