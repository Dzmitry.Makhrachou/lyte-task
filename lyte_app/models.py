from django.db import models

from lyte_app.constants import EventTicketTypes


class Organizer(models.Model):
    name = models.CharField(max_length=255)


class Event(models.Model):
    name = models.CharField(max_length=255)
    start_date = models.DateTimeField()
    organizer = models.ForeignKey(
        Organizer,
        null=True,
        on_delete=models.SET_NULL,
        related_name='events',
    )


class EventTicket(models.Model):
    type = models.PositiveSmallIntegerField(
        choices=EventTicketTypes.get_choices(),
        default=EventTicketTypes.REGULAR.value
    )
    price = models.IntegerField()
    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name='tickets',
    )
