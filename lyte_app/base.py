from enum import Enum


class BaseEnum(Enum):
    def __init__(self, *args):
        self.verbose_name, self._value_ = args[:2]

    @classmethod
    def get_choices(cls):
        return tuple((i._value_, i.verbose_name) for i in cls)
