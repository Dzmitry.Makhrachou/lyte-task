from collections import OrderedDict

from dateutil import parser
from rest_framework.mixins import (
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
)
from rest_framework.viewsets import GenericViewSet
import rest_framework_filters as filters

from lyte_app.api.v1.serializers import (
    EventReadSerializer,
    LightEventReadSerializer,
    EventUpdateSerializer,
)
from lyte_app.models import Event


class EventFilterSet(filters.FilterSet):
    start_date_after = filters.CharFilter(
        method='filter_start_date_after'
    )

    start_date_before = filters.CharFilter(
        method='filter_start_date_before'
    )

    max_ticket_price = filters.CharFilter(
        method='filter_max_ticket_price'
    )

    class Meta:
        model = Event
        fields = OrderedDict((
            ('name', ['exact', 'in']),
            ('organizer__name', ['exact', 'in']),
        ))

    def filter_start_date_after(self, qs, name, value):
        datetime = parser.parse(value)
        return qs.filter(start_date__gte=datetime)

    def filter_start_date_before(self, qs, name, value):
        datetime = parser.parse(value)
        return qs.filter(start_date__lt=datetime)

    def filter_max_ticket_price(self, qs, name, value):
        return qs.filter(
            tickets__price__lte=int(value)
        )


class EventsView(
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    queryset = Event.objects.all()
    filter_class = EventFilterSet

    def get_serializer_class(self):
        if self.action == 'list':
            return LightEventReadSerializer
        elif self.action == 'retrieve':
            return EventReadSerializer
        elif self.action == 'update':
            return EventUpdateSerializer
        return EventReadSerializer
