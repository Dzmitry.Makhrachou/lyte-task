from django.conf.urls import url

from lyte_app.api.v1 import views

urlpatterns = [
    url(r'^events/(?P<pk>\w+)', views.EventsView.as_view(
        {'get': 'retrieve', 'put': 'update'}
    )),
    url(r'^events/', views.EventsView.as_view({'get': 'list'})),
]
