from django.utils import timezone
from rest_framework import serializers

from lyte_app.models import Event, Organizer


class OrganizerReadSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(read_only=True)


class EventTicketReadSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    price = serializers.SerializerMethodField()

    def get_price(self, obj):
        return obj.price / 100.0


class BaseEventReadSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(read_only=True)
    start_date = serializers.DateTimeField(read_only=True)


class LightEventReadSerializer(BaseEventReadSerializer):
    pass


class EventReadSerializer(BaseEventReadSerializer):
    organizer = OrganizerReadSerializer()
    tickets = EventTicketReadSerializer(many=True)


class OrganizerUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255)

    class Meta:
        model = Organizer
        fields = '__all__'


class EventUpdateSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=255, required=False)
    start_date = serializers.DateTimeField(required=False)
    organizer = OrganizerUpdateSerializer(required=False)

    class Meta:
        model = Event
        fields = '__all__'

    def validate_start_date(self, value):
        if value < timezone.now():
            raise serializers.ValidationError('The date can\'t be in the past')
        return value

    def update(self, instance, validated_data):
        organizer_data = validated_data.pop('organizer', None)
        if organizer_data:
            organizer_update_serializer = self.fields['organizer']
            organizer_update_serializer.update(
                instance.organizer, organizer_data
            )
        return super().update(instance, validated_data)
